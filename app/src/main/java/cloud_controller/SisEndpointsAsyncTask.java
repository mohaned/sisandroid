package cloud_controller;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.util.Log;
import android.widget.Toast;

//import com.appspot.manarat_grading_system.sis.Sis;
//import com.appspot.manarat_grading_system.sis.model.Parent;
//import com.appspot.manarat_grading_system.sis.model.Teacher;
import com.appspot.manarat_grading_system.sism.Sism;
import com.appspot.manarat_grading_system.sism.model.TeacherClassRoom;
import com.example.mhm.myapplication.MainActivity;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SisEndpointsAsyncTask extends AsyncTask<Pair<Context, String>, Void, String> {
//        private static MyApi myApiService = null;
    private static Sism myApiService = null;
    private Context context;

    @Override
    protected String doInBackground(Pair<Context, String>... params) {
            if(myApiService == null) {  // Only do this once
                Sism.Builder builder = new Sism.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), MainActivity.credential);
                myApiService = builder.build();
            }// end if
//
            context = params[0].first;
            String name = params[0].second;

            try {
                List<TeacherClassRoom> classRooms = myApiService.getTeacherClassRoom().execute().getItems();
//                Toast.makeText(context, String.valueOf(l == null), Toast.LENGTH_SHORT).show();
//                return "mohaned";

                if(classRooms == null){
                    return "You don't have any classroom";
                }else{
                    StringBuilder sb = new StringBuilder();
                    for(TeacherClassRoom tcr: classRooms){
                        sb.append(tcr.getSubject());
                        sb.append("-");
                        sb.append(tcr.getClasse());
                        sb.append("#");
                    }

                    return sb.toString();
                }
//                return myApiService.sayHelloWithAuth().execute().getMsg();
//
//                 List<TeacherClassRoom> classRooms = myApiService.getTeacherClassRoom().execute().getItems();
//                StringBuilder sb = new StringBuilder("Class Rooms: ");
//                for(TeacherClassRoom tcr: classRooms){
//                    sb.append(tcr.getSubject());
//                    sb.append("-");
//                    sb.append(tcr.getClasse());
//                }
//
//                return sb.toString();
//                return myApiService.sayHelloWithAuth().execute().getMsg();
//                return myApiService.sayHello().execute().getMsg();

            } catch (IOException e) {
                return e.getMessage();
            }
//        return null;
    }

    @Override
    protected void onPostExecute(String result) {
            Toast.makeText(context, "Results: " + result, Toast.LENGTH_LONG).show();
    }
}