package com.example.mhm.myapplication;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.appspot.manarat_grading_system.sism.Sism;
import com.appspot.manarat_grading_system.sism.model.GradesDistribution;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentsFragment extends Fragment {

    private AssessmentsAdapter mAssessmentsAdapter;
    //    private ArrayAdapter<String> mAssessmentsAdapter;
    private static final String LOG_TAG = "AssessmentsFragment_TAG";


    public AssessmentsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final String subjectWebSafeKey = getActivity().getIntent().getStringExtra(Constants.SUBJECT_WEB_SAFE_KEY);
        final String sectionWebSafeKey = getActivity().getIntent().getStringExtra(Constants.SECTION_WEB_SAFE_KEY);

//        Toast.makeText(getActivity(), sectionWebSafeKey, Toast.LENGTH_SHORT).show();
//        Toast.makeText(getActivity(), subjectWebSafeKey, Toast.LENGTH_SHORT).show();

        mAssessmentsAdapter =
                new AssessmentsAdapter(
                        getActivity(),
                        R.id.listview_assessments,
                        new ArrayList<GradesDistribution>());

        new FetchAssessmentsTask().execute(subjectWebSafeKey);

        View rootView = inflater.inflate(R.layout.fragment_assessments, container, false);


        ListView listView = (ListView) rootView.findViewById(R.id.listview_assessments);
        listView.setAdapter(mAssessmentsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                String assessment = mAssessmentsAdapter.getItem(position).getAssessmentDisplayName();
//                Toast.makeText(getActivity(), assessment, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), AssessmentDetailActivity.class);

                intent.putExtra(Constants.SECTION_WEB_SAFE_KEY, sectionWebSafeKey);
                intent.putExtra(Constants.SUBJECT_WEB_SAFE_KEY, subjectWebSafeKey);

                intent.putExtra(
                        Constants.ASSESSTMENT_WEB_SAFE_KEY,
                        mAssessmentsAdapter.getItem(position).getWebsafeKey());

                startActivity(intent);
            }
        });


        return rootView;
    }

    public class FetchAssessmentsTask extends AsyncTask<String, Void, ArrayList<GradesDistribution>> {

        private Sism myApiService = null;

        @Override
        protected ArrayList<GradesDistribution> doInBackground(String... params) {
            if (myApiService == null) {
                Sism.Builder builder = new Sism.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), MainActivity.credential);

                myApiService = builder.build();
            }


            try {
                String webSafeKey = params[0];
//                String webSafeKey = "ahhzfm1hbmFyYXQtZ3JhZGluZy1zeXN0ZW1yNQsSBlNjaG9vbBiR4PQBDAsSBFllYXIYAQwLEgVMZXZlbBixiUsMCxIHU3ViamVjdBjbnngM";

                List<GradesDistribution> gradeDistros =
                        myApiService.getGradesDistribution(webSafeKey).execute().getItems();

                return (ArrayList<GradesDistribution>) gradeDistros;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<GradesDistribution> result) {
            if (result != null) {
                mAssessmentsAdapter.clear();
                for (GradesDistribution classroom : result) {
                    mAssessmentsAdapter.add(classroom);
                }
            }
        }
    }

}
