package com.example.mhm.myapplication;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.appspot.manarat_grading_system.sism.Sism;
import com.appspot.manarat_grading_system.sism.model.Quarter;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuartersFragment extends Fragment {

    private QuartersAdapter mQuartersAdapter;

    public QuartersFragment() {
    }

    public interface Callback {
        void onItemSelected(String quarterWebSafeKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quarters, container, false);

        mQuartersAdapter =
                new QuartersAdapter(
                        getActivity(),
                        R.layout.spinner_item,
                        new ArrayList<Quarter>());

        new FetchQuartersTask().execute();

        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_quarters);
        spinner.setAdapter(mQuartersAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Notify my activity.
                String quarterWebSafeKey = mQuartersAdapter.getItem(position).getWebsafeKey();
                ((Callback) getActivity()).onItemSelected(quarterWebSafeKey);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }

    public class FetchQuartersTask extends AsyncTask<String, Void, ArrayList<Quarter>> {

        private Sism myApiService = null;

        @Override
        protected ArrayList<Quarter> doInBackground(String... params) {
            if (myApiService == null) {
                Sism.Builder builder = new Sism.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), MainActivity.credential);

                myApiService = builder.build();
            }


            try {
                List<Quarter> quarters = myApiService.getQuartersByYear().execute().getItems();
                return (ArrayList<Quarter>) quarters;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Quarter> result) {
            if (result != null) {
                mQuartersAdapter.clear();
                for (Quarter quarter : result) {
                    mQuartersAdapter.add(quarter);
                }
            }
        }
    }


}
