package com.example.mhm.myapplication;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.appspot.manarat_grading_system.sism.Sism;
import com.appspot.manarat_grading_system.sism.model.TeacherClassRoom;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClassroomsFragment extends Fragment {


    private ClassRoomsAdapter mClassRoomsAdapter;
    private static final String LOG_TAG = "ClassroomsFragment_TAG";

    public ClassroomsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mClassRoomsAdapter =
                new ClassRoomsAdapter(
                        getActivity(),
                        R.layout.list_item_classroom,
                        new ArrayList<TeacherClassRoom>());

        new FetchClassRoomsTask().execute();

        View rootView = inflater.inflate(R.layout.fragment_classrooms, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.listview_classrooms);
        listView.setAdapter(mClassRoomsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getActivity(), AssessmentsActivity.class);

                intent.putExtra(
                        Constants.SUBJECT_WEB_SAFE_KEY,
                        mClassRoomsAdapter.getItem(position).getSubjectKey());

                intent.putExtra(
                        Constants.SECTION_WEB_SAFE_KEY,
                        mClassRoomsAdapter.getItem(position).getSectionKey());

                startActivity(intent);
            }
        });


        return rootView;
    }

    public class FetchClassRoomsTask extends AsyncTask<String, Void, ArrayList<TeacherClassRoom>> {

        private Sism myApiService = null;

        @Override
        protected ArrayList<TeacherClassRoom> doInBackground(String... params) {
            if (myApiService == null) {
                Sism.Builder builder = new Sism.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), MainActivity.credential);

                myApiService = builder.build();
            }

            try {
                List<TeacherClassRoom> classRooms = myApiService.getTeacherClassRoom().execute().getItems();
                return (ArrayList<TeacherClassRoom>) classRooms;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<TeacherClassRoom> result) {
            if (result != null) {
                mClassRoomsAdapter.clear();
                for (TeacherClassRoom classroom : result) {
                    mClassRoomsAdapter.add(classroom);
                }
            }
        }
    }

}
