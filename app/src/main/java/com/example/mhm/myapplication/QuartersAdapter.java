package com.example.mhm.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appspot.manarat_grading_system.sism.model.Quarter;

import java.util.List;

public class QuartersAdapter extends ArrayAdapter<Quarter>{
    public QuartersAdapter(Context context, int resource, List<Quarter> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Quarter q = getItem(position);
        String displayText = q.getQuarterName();

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        TextView cTextView = (TextView) convertView.findViewById(R.id.spinner_item_textview);

        cTextView.setText(displayText);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return  getView(position, convertView, parent);
    }
}
