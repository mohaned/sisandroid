package com.example.mhm.myapplication;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

//import com.appspot.manarat_grading_system.sis.Sis;
import com.appspot.manarat_grading_system.sism.Sism;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import cloud_controller.SisEndpointsAsyncTask;


public class MainActivity extends AppCompatActivity {

    SharedPreferences settings;
    public static GoogleAccountCredential credential;
    private String accountName;
    Sism service;
    public static final String PREF_ACCOUNT_NAME = "MY_GOOGLE_ACCOUNT";
    static final int REQUEST_ACCOUNT_PICKER = 2;

    public static final String TEXT_KEY = "mytext";
    public static final String LOG_TAG = "MainActivity_LOG_TAG";
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//
//        settings = getSharedPreferences("SISCloudypedia2", Context.MODE_PRIVATE);
//        settings.edit().clear().commit();

        settings = getSharedPreferences("SISCloudypedia2", 0);
        credential = GoogleAccountCredential.usingAudience(this, "server:client_id:815863178146-6i9u63oqp9mpejhvseugcolih1mbcmf3.apps.googleusercontent.com");
        setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));

        Sism.Builder builder = new Sism.Builder(
//                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(),
                AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                credential);
        service = builder.build();

        if (credential.getSelectedAccountName() != null) {            //
//            new SisEndpointsAsyncTask().execute(new Pair<Context, String>(this, editText.getText().toString()));
//            Toast.makeText(MainActivity.this, credential.getSelectedAccountName(), Toast.LENGTH_SHORT).show();
//            Toast.makeText(MainActivity.this, "Inside if", Toast.LENGTH_SHORT).show();
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new ClassroomsFragment())
                        .commit();
            }
        } else {
//            chooseAccount();
//            Toast.makeText(MainActivity.this, "Inside else", Toast.LENGTH_SHORT).show();

            chooseAccount();
            Toast.makeText(MainActivity.this, credential.getSelectedAccountName(), Toast.LENGTH_SHORT).show();

        }

    }

    public void next(View view) {
//        editText = (EditText) findViewById(R.id.edit_text);
        new SisEndpointsAsyncTask().execute(new Pair<Context, String>(this, editText.getText().toString()));
//        try {
//            String msg = service.sayHello().execute().getMsg();
//            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
//        } catch (IOException e) {
//            Toast.makeText(this, "Nothing to show", Toast.LENGTH_LONG).show();
//            e.printStackTrace();
//        }
    }

    // setSelectedAccountName definition
    private void setSelectedAccountName(String accountName) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREF_ACCOUNT_NAME, accountName);
        editor.commit();
        credential.setSelectedAccountName(accountName);
        this.accountName = accountName;
    }

    void chooseAccount() {
        startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
//        startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                if (data != null && data.getExtras() != null) {
                    String accountName =
                            data.getExtras().getString(
                                    AccountManager.KEY_ACCOUNT_NAME);

                    if (accountName != null) {
                        setSelectedAccountName(accountName);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.commit();
                        // User is authorized.
                        Toast.makeText(this, "User is authorized.", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public void clear(View view) {
//        editText = (EditText) findViewById(R.id.edit_text);
//        editText.setText("");
    }
}
