package com.example.mhm.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class AssessmentDetailActivity extends AppCompatActivity implements QuartersFragment.Callback{

    private String quarterWebSafeKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_detail);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.quarters_layout, new QuartersFragment())
                    .commit();
        }
    }

    @Override
    public void onItemSelected(String quarterWebSafeKey) {
        this.quarterWebSafeKey = quarterWebSafeKey;
        Toast.makeText(this, quarterWebSafeKey, Toast.LENGTH_LONG).show();
    }
}
