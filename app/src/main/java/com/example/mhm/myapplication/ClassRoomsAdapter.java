package com.example.mhm.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appspot.manarat_grading_system.sism.model.TeacherClassRoom;

import java.util.List;

public class ClassRoomsAdapter extends ArrayAdapter<TeacherClassRoom>{

    public ClassRoomsAdapter(Context context, int resource, List<TeacherClassRoom> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TeacherClassRoom tcr = getItem(position);
        String displayText = tcr.getSubject() + "-" + tcr.getClasse();

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_classroom, parent, false);
        }

        TextView cTextView = (TextView) convertView.findViewById(R.id.list_item_classroom_textview);

        cTextView.setText(displayText);

        return convertView;
    }
}
