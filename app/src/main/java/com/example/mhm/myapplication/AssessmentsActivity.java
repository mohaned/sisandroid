package com.example.mhm.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AssessmentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessments);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.assessments, new AssessmentsFragment())
                    .commit();
        }

    }
}
