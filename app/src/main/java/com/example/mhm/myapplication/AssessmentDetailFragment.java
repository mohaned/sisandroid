package com.example.mhm.myapplication;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appspot.manarat_grading_system.sism.Sism;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentDetailFragment extends Fragment {


    public AssessmentDetailFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_assessment_detail, container, false);
    }

    public class FetchAssessmentsDetailsTask extends AsyncTask<String, Void, ArrayList<String>> {

        private Sism myApiService = null;

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            if (myApiService == null) {
                Sism.Builder builder = new Sism.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), MainActivity.credential);

                myApiService = builder.build();
            }


            try {
                 myApiService.loadGradeTable("", "", "", "").execute();

//                return (ArrayList<GradesDistribution>) gradeDistros;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> result) {
            if (result != null) {
//                mAssessmentsAdapter.clear();
//                for (String name : result) {
//                    mAssessmentsAdapter.add(classroom);
//                }
            }
        }
    }

}
