package com.example.mhm.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appspot.manarat_grading_system.sism.model.GradesDistribution;

import java.util.List;

public class AssessmentsAdapter extends ArrayAdapter<GradesDistribution>{

    public AssessmentsAdapter(Context context, int resource, List<GradesDistribution> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GradesDistribution gd = getItem(position);
        String displayText = gd.getAssessmentDisplayName();

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_assessment, parent, false);
        }

        TextView cTextView = (TextView) convertView.findViewById(R.id.list_item_assessment_textview);

        cTextView.setText(displayText);

        return convertView;
    }
}
