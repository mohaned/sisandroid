package com.example.mhm.myapplication;

public final class Constants {
    private Constants(){}

    public static final String SUBJECT_WEB_SAFE_KEY = "subjectWebSafeKey";
    public static final String SECTION_WEB_SAFE_KEY = "sectionWebSafeKey";
    public static final String ASSESSTMENT_WEB_SAFE_KEY = "gradesDistributionWebSafeKey";
    public static final String QUARTER_WEB_SAFE_KEY = "quarterWebSafeKey";
}
